﻿using Sp_Ctx;
using System;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Xml;

namespace HestellingTicketPrint
{
    /// <summary>
    /// Interaction logic for Instellingen.xaml
    /// </summary>
    public partial class Instellingen : Window
    {
        private DBConnectie db;
        public Instellingen(DBConnectie con)
        {
            InitializeComponent();
            db = con;
            GetDrempelwaarde();
        }

        private void GetDrempelwaarde()
        {
            try
            {
                //string basepath = System.AppDomain.CurrentDomain.BaseDirectory;
                //if (basepath.Contains("\\bin\\Debug"))
                //    basepath = basepath.Replace("\\bin\\Debug", "");
                //if (basepath.Contains("\\bin\\Release"))
                //    basepath = basepath.Replace("\\bin\\Release", "");

                //Uri initPath = new Uri(basepath + "XML/instellingen.xml"); 
                if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "TicketPrintApp/instellingen.xml"))
                    db.GetTemplateFromSharePoint("instellingen.xml", "6", "Templates");
                Uri initPath = new Uri(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "TicketPrintApp/instellingen.xml"));

                XmlTextReader reader = new XmlTextReader(initPath.LocalPath);
                XmlNodeType type;
                while (reader.Read())
                {
                    type = reader.NodeType;
                    if (type == XmlNodeType.Element)
                    {
                        if (reader.Name == "drempelwaarde")
                        {
                            reader.Read();
                            DrempelWaardeTextBox.Text = reader.Value;
                        }

                        if (reader.Name == "printernaam")
                        {
                            reader.Read();
                            printernameTextBox.Text = reader.Value;
                        }

                    }
                }
                reader.Close();
                OpslaanButton.IsEnabled = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void OpslaanButton_Click(object sender, RoutedEventArgs e)
        {
            SaveDrempelWaarde();
        }

        private void SaveDrempelWaarde()
        {
            String drempelWaarde = DrempelWaardeTextBox.Text;
            String printernaamWaarde = printernameTextBox.Text;
            try
            {
                //string basepath = System.AppDomain.CurrentDomain.BaseDirectory;
                //if (basepath.Contains("\\bin\\Debug"))
                //    basepath = basepath.Replace("\\bin\\Debug", "");
                //if (basepath.Contains("\\bin\\Release"))
                //    basepath = basepath.Replace("\\bin\\Release", "");

                //Uri initPath = new Uri(basepath + "XML/instellingen.xml"); 
                if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "TicketPrintApp/instellingen.xml"))
                    db.GetTemplateFromSharePoint("instellingen.xml", "6", "Templates");
                Uri initPath = new Uri(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "TicketPrintApp/instellingen.xml"));

                XmlDocument instellingen = new XmlDocument();
                instellingen.Load(initPath.LocalPath);
                XmlNode DrempelWaarde = instellingen.SelectSingleNode("//drempelwaarde");
                DrempelWaarde.InnerXml = drempelWaarde;

                XmlNode printernaam = instellingen.SelectSingleNode("//printernaam");
                printernaam.InnerXml = printernaamWaarde;

                instellingen.Save(initPath.LocalPath);

                OpslaanButton.IsEnabled = false;
                this.DialogResult = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Probleem bij het opslaan van de instellingen!\n" + ex.Message);
            }
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            OpslaanButton.IsEnabled = true;
        }
    }
}
