﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Printing;

namespace HestellingTicketPrint
{
    /// <summary>
    /// Interaction logic for Afdrukvoorbeeld.xaml
    /// </summary>
    public partial class Afdrukvoorbeeld : Window
    {
        private FlowDocument flowDocument { get; set; }

        public Afdrukvoorbeeld()
        {
            InitializeComponent();
        }

        public FlowDocument AfdrukFlowDocument
        {
            get { return flowDocument; }
            set
            {
                flowDocument = value;
                AfdrukDocument = flowDocument;
            }
        }

        public IDocumentPaginatorSource AfdrukDocument
        {
            get { return PrintPreview.Document; }
            set { PrintPreview.Document = value; }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PrintDialog printdlg = new PrintDialog();
                printdlg.PageRangeSelection = PageRangeSelection.AllPages;
                printdlg.UserPageRangeEnabled = true;
                if (printdlg.ShowDialog() == true)
                {
                    PrintCapabilities capabilities = printdlg.PrintQueue.GetPrintCapabilities(printdlg.PrintTicket);
                    AfdrukFlowDocument.PageHeight = printdlg.PrintableAreaHeight - 15;
                    AfdrukFlowDocument.PageWidth = printdlg.PrintableAreaWidth;
                    AfdrukFlowDocument.PagePadding = new Thickness(15, 10, 15, 0);
                    IDocumentPaginatorSource idp = AfdrukFlowDocument as IDocumentPaginatorSource;
                    printdlg.PrintDocument(idp.DocumentPaginator, "Ticket");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Fout bij het printen!\n" + ex.Message);
            }
        }
    }
}
