﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media.Imaging;
using HerstellingTicketLibrary;
using Sp_Ctx;
using System.Printing;
using System.Xml;
using System.IO;
using System.Drawing.Printing;
using System.Deployment.Application;

namespace HestellingTicketPrint
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DBConnectie db;
        private PrintInfo info;
        private Drempelwaarden _waarden;
        public MainWindow()
        {
            InitializeComponent();
            try
            {            
                if (db == null)
                    db = new Sp_Ctx.DBConnectie();
                PrintDatumDAtePicker.Text = DateTime.Today.ToShortDateString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void VulTicketLijst()
        {
            try
            {
                String counter = _waarden.TicketWaarde;
                if (db != null)
                {
                    List<Ticket> tickets = new List<Ticket>();
                    if (klantenComboBox.SelectedIndex > 0)
                    {
                        Klant klant = (Klant)klantenComboBox.SelectedItem;
                        tickets = db.GetKlantTickets(klant.ID, _waarden.TicketWaarde);
                    }
                    else
                    {
                        tickets = db.GetActiveTickets(counter);
                    }
                    if (tickets != null || tickets.Count > 0)
                    {
                        TicketComboBox.ItemsSource = tickets;
                        TicketComboBox.DisplayMemberPath = "ID";
                        TicketComboBox.SelectedIndex = 1;
                    }
                    else
                        MessageBox.Show("Geen tickets verkregen.");
                }
                else
                    MessageBox.Show("Geen SharePoint connectie");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SluitButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                versionLabel.Content = System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
            }
            catch (InvalidDeploymentException)
            {
                versionLabel.Content = "Versie nog niet geïnstalleerd";
            }
            _waarden = db.GetDrempelwaarden();
            List<Klant> klanten = db.GetKlanten();
            Klant leeg = new Klant();
            leeg.ID = 0;
            leeg.Bedrijf = "Kies een klant om te filteren";
            klanten.Insert(0, leeg);
            klantenComboBox.ItemsSource = klanten;
            klantenComboBox.DisplayMemberPath = "Bedrijf";
            VulTicketLijst();
        }

        private void TicketComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            ComboBox combo = (ComboBox)sender;
            if (combo.Items.Count > 0 && combo.SelectedIndex > -1)
            {
                Ticket ticket = combo.SelectedItem as Ticket;
                VulTicketGegevensIn(ticket);
            }
            else
            {
                ResetForm();
            }
        }

        private void ResetForm()
        {
            Klant klant = new Klant();
            Contact contact = new Contact();
            KlantLabel.Content = "";
            ContactLabel.Content = "";
            FoutmeldingTextBlock.Text = "";
            TelLabel.Text = "";
            TypeLabel.Content = "";
            opmTicketTextBox.Text = "";
        }

        private void VulTicketGegevensIn(Ticket ticket)
        {
            Klant klant = new Klant();
            Contact contact = new Contact();
            if (ticket != null)
            {
                klant = db.GetKlantByID(ticket.KlantID);
                contact = db.GetContactByID(ticket.ContactID);


                InitPrintInfo(klant, contact, ticket);
                KlantLabel.Content = klant.Bedrijf;
                ContactLabel.Content = contact.Naam;
                FoutmeldingTextBlock.Text = ticket.Omschrijving;
                StringBuilder info = new StringBuilder();
                if (contact.Telefoon != null)
                    if (contact.Telefoon.Length >= 9)
                        info.Append(contact.Telefoon + System.Environment.NewLine);
                if (contact.GSM != null)
                    if (contact.GSM.Length >= 10)
                        info.Append(contact.GSM + System.Environment.NewLine);
                if (contact.Email != null)
                    if (contact.Email.Contains('@'))
                        info.Append(contact.Email);
                TelLabel.Text = info.ToString();
                TypeLabel.Content = ticket.Type;
                opmTicketTextBox.Text = "";
            }
        }

        private void InitPrintInfo(Klant klant, Contact contact, Ticket ticket)
        {
            info = new PrintInfo();
            info.Type = ticket.Type;
            info.Ticketnummer = ticket.ID.ToString();
            info.Datum = DateTime.Today;
            info.Klant = klant.Bedrijf;
            info.Contact = contact.Naam;
            info.ContactTelefoon = contact.Telefoon;
            info.ContactGSM = contact.GSM;
            info.ContactEmail = contact.Email;
            info.Foutmelding = ticket.Omschrijving;           
        }

        private void PrintButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(opmTicketTextBox.Text))
                    info.Opmerking = opmTicketTextBox.Text;

                

                String currentPrinter = "";
                string printername = "EPSON TM-T88IV op planning-hp";

                if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "TicketPrintApp/instellingen.xml"))
                    db.GetTemplateFromSharePoint("instellingen.xml", "6", "Templates");
                Uri initPath = new Uri(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "TicketPrintApp/instellingen.xml"));

                XmlTextReader reader = new XmlTextReader(initPath.LocalPath);
                XmlNodeType type;
                while (reader.Read())
                {
                    type = reader.NodeType;
                    if (type == XmlNodeType.Element)
                    {
                        if (reader.Name == "printernaam")
                        {
                            reader.Read();
                            printername = reader.Value;
                        }
                    }
                }
                reader.Close();

                if (HerstellingTicketLibrary.PrinterSettings.IsPrinterInstalled(printername)) //"EPSON TM-T88IV Receipt"
                {
                    System.Drawing.Printing.PrinterSettings settings = new System.Drawing.Printing.PrinterSettings();
                    foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
                    {
                        settings.PrinterName = printer;
                        //PaperSource source = 
                        //settings.PaperSources.
                        if (settings.IsDefaultPrinter)
                            currentPrinter = printer;
                    }

                    HerstellingTicketLibrary.PrinterSettings.SetDefaultPrinter(printername);//"EPSON TM-T88IV Receipt"
                }
                PrintTheTicket();
                if (currentPrinter != "")
                    HerstellingTicketLibrary.PrinterSettings.SetDefaultPrinter(currentPrinter);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Print(ex.Message);
                MessageBox.Show("Fout bij het printen!!\n" + ex.Message);
            }
        }

        private void PrintTheTicket()
        {
            object oMissing = System.Reflection.Missing.Value;
            string path;
            string documentnaam = "HerstellingDocTemplate.docx";
            //if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "TicketPrintApp/" + documentnaam))
            path = db.GetTemplateFromSharePoint(documentnaam, "5", "Templates");
            object template = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "TicketPrintApp/" + documentnaam);
            WordDocument doc = new WordDocument(template, "Staand");
            List<string> printinfo = new List<string>();
            printinfo.Add(info.Type);
            printinfo.Add(info.Ticketnummer);
            printinfo.Add(info.Datum.ToShortDateString());
            printinfo.Add(info.Klant);
            printinfo.Add(info.Contact);
            printinfo.Add(info.ContactTelefoon);
            printinfo.Add(info.ContactGSM);
            printinfo.Add(info.ContactEmail);
            printinfo.Add(info.Foutmelding);
            if (!String.IsNullOrEmpty(info.Opmerking))
            {
                printinfo.Add(info.Opmerking);
            }
            else
            {
                printinfo.Add("");
            }
            doc.Activate();
            doc.ChangeTicketFields(printinfo);        
            //doc.Print();
            //doc.quit();           
        }

        private FlowDocument StelTicketAfdrukSamen()
        {
            FlowDocument document = new FlowDocument();
            document.FontSize = 13.0;

            if (!String.IsNullOrEmpty(opmTicketTextBox.Text))
                info.Opmerking = opmTicketTextBox.Text;

            Paragraph myParagraph = new Paragraph();
            myParagraph.Inlines.Add(new Bold(new Run(info.Type + " - " + info.Ticketnummer + "\n\n")));
            myParagraph.Inlines.Add(new Bold(new Run("Datum:\n")));
            myParagraph.Inlines.Add(new Run(info.Datum.ToShortDateString() + "\n\n"));
            myParagraph.Inlines.Add(new Bold(new Run("Klant:\n")));
            String[] klantinfo = info.Klant.Split('-');
            foreach (String k in klantinfo)
            {
                myParagraph.Inlines.Add(new Run(k + "\n"));
            }
            myParagraph.Inlines.Add(new Bold(new Run("\nContact:\n")));
            if (!String.IsNullOrEmpty(info.Contact))
                myParagraph.Inlines.Add(new Run(info.Contact + "\n"));
            if (!String.IsNullOrEmpty(info.ContactTelefoon))
                myParagraph.Inlines.Add(new Run(info.ContactTelefoon + "\n"));
            if (!String.IsNullOrEmpty(info.ContactGSM))
                myParagraph.Inlines.Add(new Run(info.ContactGSM + "\n"));
            if (!String.IsNullOrEmpty(info.ContactEmail))
                myParagraph.Inlines.Add(new Run(info.ContactEmail + "\n"));
            myParagraph.Inlines.Add(new Run("\n"));
            myParagraph.Inlines.Add(new Bold(new Run("Foutmelding:\n")));
            if (info.Foutmelding.Length > 40)
            {
                string text = info.Foutmelding;
                while (text.Length > 40)
                {
                    int lengte = 40;
                    while (text[lengte] != ' ' && (lengte + 1) < text.Length)
                    {
                        lengte++;
                    }
                    string lijn = text.Substring(0, lengte + 1);
                    text = text.Substring(lengte + 1);

                    myParagraph.Inlines.Add(lijn + "\n");
                }
                myParagraph.Inlines.Add(text);
            }
            else
                myParagraph.Inlines.Add(info.Foutmelding + "\n");
            

            myParagraph.Inlines.Add(new Run("\n"));
            if (!String.IsNullOrEmpty(info.Opmerking))
            {
                myParagraph.Inlines.Add(new Bold(new Run("Opmerking:\n")));
                if (info.Opmerking.Length > 40)
                {
                    string text = info.Opmerking;
                    while (text.Length > 40)
                    {
                        int lengte = 40;
                        while (text[lengte] != ' ' && (lengte + 1) < text.Length)
                        {
                            lengte++;
                        }
                        string lijn = text.Substring(0, lengte + 1);
                        text = text.Substring(lengte + 1);

                        myParagraph.Inlines.Add(lijn + "\n");
                    }
                    myParagraph.Inlines.Add(text);
                }
                else
                    myParagraph.Inlines.Add(info.Opmerking + "\n\n");
            }
            else
                myParagraph.Inlines.Add("\n\n");
            myParagraph.Inlines.Add(new Run("\n-"));

            Image photo = new Image();
            BitmapImage photoFile = new BitmapImage();
            String photourl = "pack://application:,,,/Images/ITC_logo.gif";
            photoFile.BeginInit();
            photoFile.UriSource = new Uri(photourl, UriKind.Absolute);
            photoFile.EndInit();
            photo.Source = photoFile;
            photo.Width = 100;
            photo.Height = 100;
            photo.Margin = new Thickness(68, 0, 0, 10);
            Paragraph pPhoto = new Paragraph();
            pPhoto.Inlines.Add(photo);

            System.Windows.Documents.Paragraph para = new System.Windows.Documents.Paragraph();
            document.Blocks.Add(pPhoto);
            document.Blocks.Add(myParagraph);
            return document;
        }

        private void reloadButton_Click(object sender, RoutedEventArgs e)
        {
            VulTicketLijst();
        }

        private void InstellingenButton_Click(object sender, RoutedEventArgs e)
        {
            Instellingen inst = new Instellingen(db);
            inst.ShowDialog();
        }

        private void klantenComboBox_DropDownClosed(object sender, EventArgs e)
        {          
                VulTicketLijst();
        }
    }
}
