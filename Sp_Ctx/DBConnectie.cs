﻿using Microsoft.SharePoint.Client;
using MSDN.Samples.ClaimsAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HerstellingTicketLibrary;
using System.ComponentModel;

namespace Sp_Ctx
{
    [DataObject(true)]
    public class DBConnectie
    {

        private static ClientContext ctx;

        public DBConnectie()
        {
            try
            {
                //string targetSite = "https://itcbe.sharepoint.com/";

                using (ctx = ClaimClientContext.GetAuthenticatedContext("https://itcbe.sharepoint.com/"))
                {
                    if (ctx != null)
                    {
                        ctx.Load(ctx.Web); // Query for Web
                        ctx.ExecuteQuery(); // Execute
                        //Console.WriteLine(ctx.Web.Title);
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Kan geen connectie maken met SharePoint.\n" + ex.Message);
            }

        }

        /******************************** Interne telefoon *********************************/

        [DataObjectMethod(DataObjectMethodType.Select)]
        public List<Telefoon> GetTelefoon()
        {
            if (ctx == null)
                return null;
            List<Telefoon> telefoonLijst = new List<Telefoon>();
            List myList = ctx.Web.Lists.GetByTitle("Interne telefoon");

            StringBuilder caml = new StringBuilder();
            caml.Append("<View><Query>");
            caml.Append("</Query><RowLimit>100</RowLimit></View>");

            CamlQuery query = new CamlQuery();
            query.ViewXml = caml.ToString();

            ListItemCollection myItems = myList.GetItems(query);
            ctx.Load(myItems);
            ctx.ExecuteQuery();

            foreach (var item in myItems)
            {
                telefoonLijst.Add(new Telefoon(item["Title"].ToString(), item["Persoon"].ToString()));
            }


            return telefoonLijst;
        }

        //Insert new telefoon
        [DataObjectMethod(DataObjectMethodType.Insert)]
        public Boolean AddTelefoon(Telefoon telefoon)
        {
            if (ctx == null)
                return false;
            try
            {
                List list = ctx.Web.Lists.GetByTitle("Interne telefoon");
                ListItemCreationInformation itemCreateInfo = new ListItemCreationInformation();
                ListItem item = list.AddItem(itemCreateInfo);
                item["Title"] = telefoon.Nummer.ToString();
                item["Persoon"] = telefoon.Persoon.ToString();
                item.Update();
                ctx.ExecuteQuery();
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public Drempelwaarden GetDrempelwaarden()
        {
            if (ctx == null)
                return null;
            try
            {
                Drempelwaarden waarden = new Drempelwaarden();
                List myList = ctx.Web.Lists.GetByTitle("Drempelwaarden");

                StringBuilder caml = new StringBuilder();

                CamlQuery query = new CamlQuery();
                caml.Append("<View><Query>");
                caml.Append("</Query>");
                caml.Append("<ViewFields>");
                caml.Append("<FieldRef Name='ID' />");
                caml.Append("<FieldRef Name='drempelwaarde' />");
                caml.Append("<FieldRef Name='Title' />");
                caml.Append("</ViewFields>");
                caml.Append("</View>");

                query.ViewXml = caml.ToString();

                ListItemCollection myItems = myList.GetItems(query);
                ctx.Load(myItems);
                ctx.ExecuteQuery();


                foreach (var item in myItems)
                {
                    if (item["Title"].ToString() == "Tijdsregistratie ticket")
                        waarden.TijdsregistratieWaarde = item["drempelwaarde"].ToString();
                    else
                        waarden.TicketWaarde = item["drempelwaarde"].ToString();
                }

                return waarden;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string GetTemplateFromSharePoint(string filename, string id, string listname)
        {
            if (ctx == null)
                throw new Exception("Geen connectie met SharePoint!");
            try
            {
                string filepath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\TicketPrintApp\\";
                if (!System.IO.Directory.Exists(filepath))
                {
                    System.IO.Directory.CreateDirectory(filepath);
                }
                var list = ctx.Web.Lists.GetByTitle(listname);
                var file = list.GetItemById(id);
                ctx.Load(list);
                ctx.Load(file, i => i.File);
                ctx.ExecuteQuery();


                var fileref = file.File.ServerRelativeUrl;
                using (FileInformation fileInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(ctx, fileref))
                {
                    using (var stream = System.IO.File.Create(filepath + filename))
                    {
                        fileInfo.Stream.CopyTo(stream);
                    }
                }

                return filepath + filename;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Ophalen van de active tickets
        /// </summary>
        /// <returns></returns>
        public List<Ticket> GetActiveTickets(String counter)
        {
            if (ctx == null)
                return null;
            try
            {
                List<Ticket> tickets = new List<Ticket>();
                List myList = ctx.Web.Lists.GetByTitle("Ticket");

                StringBuilder caml = new StringBuilder();

                CamlQuery query = new CamlQuery();
                caml.Append("<View><Query>");
                caml.Append("<Where>");
                caml.Append("<And>");
                caml.Append("<Gt><FieldRef Name='ID' /><Value Type='Counter'>" + counter + "</Value></Gt>");
                caml.Append("<Neq><FieldRef Name='Status' /><Value Type='Choice'>Afgesloten</Value></Neq>");
                caml.Append("</And>");
                caml.Append("</Where>");
                caml.Append("</Query>");
                caml.Append("<ViewFields>");
                caml.Append("<FieldRef Name='ID' />");
                caml.Append("<FieldRef Name='Klant' />");
                caml.Append("<FieldRef Name='Contact' />");
                caml.Append("<FieldRef Name='Title' />");
                caml.Append("<FieldRef Name='Type_x0020_ticket' />");
                caml.Append("</ViewFields>");
                caml.Append("<RowLimit>4900</RowLimit>");
                caml.Append("</View>");

                query.ViewXml = caml.ToString();

                ListItemCollection myItems = myList.GetItems(query);
                ctx.Load(myItems);
                ctx.ExecuteQuery();

                foreach (var item in myItems)
                {
                    Ticket ticket = new Ticket();
                    FieldLookupValue Klantfl = (FieldLookupValue)item["Klant"];
                    //Klant klant = GetKlantByLookup(Klantfl);
                    FieldLookupValue Contactfl = (FieldLookupValue)item["Contact"];
                    //Contact contact = GetContactByLookup(Contactfl);

                    ticket.ID = Convert.ToInt32(item["ID"]);
                    ticket.KlantID = Klantfl.LookupId;
                    ticket.ContactID = Contactfl.LookupId;
                    //if (klant != null)
                    //    ticket.Klant = klant;
                    //if (contact != null)
                    //    ticket.Contact = contact;
                    ticket.Type = item["Type_x0020_ticket"].ToString();
                    ticket.Omschrijving = item["Title"].ToString();

                    tickets.Add(ticket);
                }
                return tickets;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Contact GetContactByID(Int32 id)
        {
            if (ctx == null)
                return null;
            try
            {
                List<Ticket> tickets = new List<Ticket>();
                List myList = ctx.Web.Lists.GetByTitle("Contactpersonen");

                //Int32 contactid = Contactfl.LookupId;

                StringBuilder caml = new StringBuilder();

                CamlQuery query = new CamlQuery();
                caml.Append("<View><Query>");
                caml.Append("<Where>");
                caml.Append("<Eq><FieldRef Name='ID' /><Value Type='Counter'>" + id + "</Value></Eq>");
                caml.Append("</Where>");
                caml.Append("</Query>");
                caml.Append("<ViewFields>");
                caml.Append("<FieldRef Name='ID' />");
                caml.Append("<FieldRef Name='Naam' />");
                caml.Append("<FieldRef Name='Title' />");
                caml.Append("<FieldRef Name='FirstName' />");
                caml.Append("<FieldRef Name='WorkPhone' />");
                caml.Append("<FieldRef Name='CellPhone' />");
                caml.Append("<FieldRef Name='Email' />");
                caml.Append("</ViewFields>");
                caml.Append("<RowLimit>1</RowLimit>");
                caml.Append("</View>");

                query.ViewXml = caml.ToString();

                ListItemCollection myItems = myList.GetItems(query);
                ctx.Load(myItems);
                ctx.ExecuteQuery();

                Contact contact = new Contact();

                foreach (var item in myItems)
                {
                    contact.ID = Convert.ToInt32(item["ID"]);
                    contact.Naam = item["Title"].ToString() + " " + item["FirstName"].ToString(); 
                    if (item["WorkPhone"] != null)
                        contact.Telefoon = item["WorkPhone"].ToString();
                    if (item["CellPhone"] != null)
                        contact.GSM = item["CellPhone"].ToString();
                    if (item["Email"] != null)
                    {
                        string email = item["Email"].ToString();
                        if (email.Contains("<div"))
                            email = StripHTML(email);
                        contact.Email = email;
                    }
                }

                return contact;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private string StripHTML(string email)
        {
            String result = "";
            String[] StrArray = email.Split('>');
            foreach (String item in StrArray)
            {
                if (item.Contains('@'))
                {
                    String[] StrMail = item.Split('<');
                    foreach (String HTMLitem in StrMail)
                    {
                        if (HTMLitem.Contains('@'))
                        {
                            result = HTMLitem;
                            if (result.Contains("mailto"))
                            {
                                String[] arr = result.Split(';');
                                foreach (String arritem in arr)
                                {
                                    if (arritem.Contains('@'))
                                    {
                                        result = arritem;
                                        if (result.Contains('\"'))
                                        {
                                            result = result.Substring(0, result.IndexOf('\"'));
                                        }
                                        break;
                                    }
                                }
                            }
                            break;
                        }
                    }
                    break;
                }
            }

            return result;
        }

        private Contact GetContactByLookup(FieldLookupValue Contactfl)
        {
            if (ctx == null)
                return null;
            try
            {
                List<Ticket> tickets = new List<Ticket>();
                List myList = ctx.Web.Lists.GetByTitle("Contactpersonen");

                Int32 contactid = Contactfl.LookupId;

                StringBuilder caml = new StringBuilder();

                CamlQuery query = new CamlQuery();
                caml.Append("<View><Query>");
                caml.Append("<Where>");
                caml.Append("<Eq><FieldRef Name='ID' /><Value Type='Counter'>" + contactid + "</Value></Eq>");
                caml.Append("</Where>");
                caml.Append("</Query>");
                caml.Append("<ViewFields>");
                caml.Append("<FieldRef Name='ID' />");
                caml.Append("<FieldRef Name='Naam' />");
                caml.Append("<FieldRef Name='WorkPhone' />");
                caml.Append("<FieldRef Name='CellPhone' />");
                caml.Append("<FieldRef Name='Email' />");
                caml.Append("</ViewFields>");
                caml.Append("<RowLimit>1</RowLimit>");
                caml.Append("</View>");

                query.ViewXml = caml.ToString();

                ListItemCollection myItems = myList.GetItems(query);
                ctx.Load(myItems);
                ctx.ExecuteQuery();

                Contact contact = new Contact();

                foreach (var item in myItems)
                {
                    contact.ID = Convert.ToInt32(item["ID"]);
                    if (item["Naam"] != null)
                        contact.Naam = item["Naam"].ToString();
                    if (item["WorkPhone"] != null)
                        contact.Telefoon = item["WorkPhone"].ToString();
                    if (item["CellPhone"] != null)
                        contact.GSM = item["CellPhone"].ToString();
                    if (item["Email"] != null)
                        contact.Email = item["Email"].ToString();
                }

                return contact;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Ticket> GetKlantTickets(int iD, string counter)
        {
            if (ctx == null)
                return null;
            try
            {
                List<Ticket> tickets = new List<Ticket>();
                List myList = ctx.Web.Lists.GetByTitle("Ticket");

                StringBuilder caml = new StringBuilder();

                CamlQuery query = new CamlQuery();
                caml.Append("<View><Query>");
                caml.Append("<Where>");
                caml.Append("<And><And>");
                caml.Append("<Gt><FieldRef Name='ID' /><Value Type='Counter'>" + counter + "</Value></Gt>");
                caml.Append("<Neq><FieldRef Name='Status' /><Value Type='Choice'>Afgesloten</Value></Neq>");
                caml.Append("</And>");
                caml.Append("<Eq><FieldRef Name='Klant' LookupId='True'/><Value Type='Lookup'>" + iD + "</Value></Eq>");
                caml.Append("</And>");
                caml.Append("</Where>");
                caml.Append("</Query>");
                caml.Append("<ViewFields>");
                caml.Append("<FieldRef Name='ID' />");
                caml.Append("<FieldRef Name='Klant' />");
                caml.Append("<FieldRef Name='Contact' />");
                caml.Append("<FieldRef Name='Title' />");
                caml.Append("<FieldRef Name='Type_x0020_ticket' />");
                caml.Append("</ViewFields>");
                caml.Append("<RowLimit>4900</RowLimit>");
                caml.Append("</View>");

                query.ViewXml = caml.ToString();

                ListItemCollection myItems = myList.GetItems(query);
                ctx.Load(myItems);
                ctx.ExecuteQuery();

                foreach (var item in myItems)
                {
                    Ticket ticket = new Ticket();
                    FieldLookupValue Klantfl = (FieldLookupValue)item["Klant"];
                    //Klant klant = GetKlantByLookup(Klantfl);
                    FieldLookupValue Contactfl = (FieldLookupValue)item["Contact"];
                    //Contact contact = GetContactByLookup(Contactfl);

                    ticket.ID = Convert.ToInt32(item["ID"]);
                    ticket.KlantID = Klantfl.LookupId;
                    ticket.ContactID = Contactfl.LookupId;
                    //if (klant != null)
                    //    ticket.Klant = klant;
                    //if (contact != null)
                    //    ticket.Contact = contact;
                    ticket.Type = item["Type_x0020_ticket"].ToString();
                    ticket.Omschrijving = item["Title"].ToString();

                    tickets.Add(ticket);
                }
                return tickets;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Klant GetKlantByID(Int32 id)
        {
            if (ctx == null)
                return null;
            try
            {
                List<Ticket> tickets = new List<Ticket>();
                List myList = ctx.Web.Lists.GetByTitle("Klantenlijst");

                //Int32 klantid = Klantfl.LookupId;

                StringBuilder caml = new StringBuilder();

                CamlQuery query = new CamlQuery();
                caml.Append("<View><Query>");
                caml.Append("<Where>");
                caml.Append("<Eq><FieldRef Name='ID' /><Value Type='Counter'>" + id + "</Value></Eq>");
                caml.Append("</Where>");
                caml.Append("</Query>");
                caml.Append("<ViewFields>");
                caml.Append("<FieldRef Name='ID' />");
                caml.Append("<FieldRef Name='Naam1' />");
                caml.Append("<FieldRef Name='Locatie' />");
                caml.Append("<FieldRef Name='Telefoon' />");
                caml.Append("</ViewFields>");
                caml.Append("<RowLimit>1</RowLimit>");
                caml.Append("</View>");

                query.ViewXml = caml.ToString();

                ListItemCollection myItems = myList.GetItems(query);
                ctx.Load(myItems);
                ctx.ExecuteQuery();

                Klant klant = new Klant();

                foreach (var item in myItems)
                {
                    klant.ID = Convert.ToInt32(item["ID"]);
                    if (item["Naam1"] != null)
                        klant.Bedrijf = item["Naam1"].ToString() + " - " + item["Locatie"].ToString().ToUpper();
                    if (item["Telefoon"] != null)
                        klant.Telefoon = item["Telefoon"].ToString();
                }

                return klant;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public List<Klant> GetKlanten()
        {
            if (ctx == null)
                return null;
            try
            {
                List<Klant> klanten = new List<Klant>();
                List myList = ctx.Web.Lists.GetByTitle("Klantenlijst");

                //Int32 klantid = Klantfl.LookupId;

                StringBuilder caml = new StringBuilder();

                CamlQuery query = new CamlQuery();
                caml.Append("<View><Query>");
                caml.Append("<Where>");
                caml.Append("<Neq><FieldRef Name='Archief' /><Value Type='Boolean'>1</Value></Neq>");
                caml.Append("</Where>");
                caml.Append("<OrderBy><FieldRef Name='Naam1' /></OrderBy>");
                caml.Append("</Query>");
                caml.Append("<ViewFields>");
                caml.Append("<FieldRef Name='ID' />");
                caml.Append("<FieldRef Name='Naam1' />");
                caml.Append("<FieldRef Name='Locatie' />");
                caml.Append("<FieldRef Name='Telefoon' />");
                caml.Append("</ViewFields>");
                caml.Append("</View>");

                query.ViewXml = caml.ToString();

                ListItemCollection myItems = myList.GetItems(query);
                ctx.Load(myItems);
                ctx.ExecuteQuery();


                foreach (var item in myItems)
                {
                    Klant klant = new Klant();
                    klant.ID = Convert.ToInt32(item["ID"]);
                    if (item["Naam1"] != null)
                        klant.Bedrijf = item["Naam1"].ToString() + " - " + item["Locatie"].ToString().ToUpper();
                    if (item["Telefoon"] != null)
                        klant.Telefoon = item["Telefoon"].ToString();
                    klanten.Add(klant);
                }

                return klanten;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private Klant GetKlantByLookup(FieldLookupValue Klantfl)
        {
            if (ctx == null)
                return null;
            try
            {
                List<Ticket> tickets = new List<Ticket>();
                List myList = ctx.Web.Lists.GetByTitle("Klantenlijst");

                Int32 klantid = Klantfl.LookupId;

                StringBuilder caml = new StringBuilder();

                CamlQuery query = new CamlQuery();
                caml.Append("<View><Query>");
                caml.Append("<Where>");
                caml.Append("<Eq><FieldRef Name='ID' /><Value Type='Counter'>" + klantid + "</Value></Eq>");
                caml.Append("</Where>");
                caml.Append("</Query>");
                caml.Append("<ViewFields>");
                caml.Append("<FieldRef Name='ID' />");
                caml.Append("<FieldRef Name='Volledige_x0020_klantennaam' />");
                caml.Append("<FieldRef Name='Telefoon' />");
                caml.Append("</ViewFields>");
                caml.Append("<RowLimit>1</RowLimit>");
                caml.Append("</View>");

                query.ViewXml = caml.ToString();

                ListItemCollection myItems = myList.GetItems(query);
                ctx.Load(myItems);
                ctx.ExecuteQuery();

                Klant klant = new Klant();

                foreach (var item in myItems)
                {
                    klant.ID = Convert.ToInt32(item["ID"]);
                    if (item["Volledige_x0020_klantennaam"] != null)
                        klant.Bedrijf = item["Volledige_x0020_klantennaam"].ToString();
                    if (item["Telefoon"] != null)
                        klant.Telefoon = item["Telefoon"].ToString();
                }

                return klant;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
    }
}

//        /************************************ VOERTUIGEN **********************************/

//        public ObservableCollection<VoertuigReservatieInfo> GetVoertuigen(String verhuurlocatieId)
//        {
//            if (ctx == null)
//                return null;

//            ObservableCollection<VoertuigReservatieInfo> voertuigen = new ObservableCollection<VoertuigReservatieInfo>();
//            List myList = ctx.Web.Lists.GetByTitle("Voertuigen");

//            StringBuilder caml = new StringBuilder();
//            caml.Append("<View><Query>");
//            caml.Append("<Where>");
//            //caml.Append("<And>");
//            caml.Append("<Eq><FieldRef Name='Actief'/>");
//            caml.Append("<Value Type='Integer'>1</Value></Eq>");
//            //caml.Append("<Eq><FieldRef Name='Thuislocatie' LookupId='True'/>");
//            //caml.Append("<Value Type='Lookup'>" + verhuurlocatieId + "</Value></Eq>");
//            //caml.Append("</And>");
//            caml.Append("</Where>");
//            /*caml.Append("<OrderBy>");
//            caml.Append("<FieldRef Name='Voertuig'/>");
//            caml.Append("<FieldRef Name='Type_x0020_voertuig'/>");
//            caml.Append("</OrderBy>");*/
//            caml.Append("<GroupBy>");
//            caml.Append("<FieldRef Name='Voertuig'/>");
//            caml.Append("<FieldRef Name='Type_x0020_voertuig'/>");
//            caml.Append("</GroupBy>");
//            caml.Append("</Query><RowLimit>100</RowLimit></View>");

//            CamlQuery query = new CamlQuery();
//            query.ViewXml = caml.ToString();

//            ListItemCollection myItems = myList.GetItems(query);
//            ctx.Load(myItems);
//            ctx.ExecuteQuery();

//            Console.WriteLine("VoertuigReservatieInfoLijst opbouwen ...");
//            String huidigVoertuigNaam = "";
//            VoertuigReservatieInfo huidigVoertuig = null;
//            foreach (ListItem myItem in myItems)
//            {
//                String voertuigNaam = myItem.FieldValues["Voertuig"] + " " + myItem.FieldValues["Type_x0020_voertuig"];
//                if (!voertuigNaam.Equals(huidigVoertuigNaam))// er is een nieuw type voertuig
//                {
//                    if (huidigVoertuig != null)//informatie van een voertuig opgebouwd, toevoegen aan returnlijst
//                        voertuigen.Add(huidigVoertuig);
//                    //nieuw voertuig (gedetecteerd), want waarden verschillen
//                    huidigVoertuig = new VoertuigReservatieInfo(voertuigNaam, myItem.FieldValues["Huurprijs"].ToString(), myItem.FieldValues["Waarborg"].ToString(), myItem.FieldValues["Aantal_x0020_personen"].ToString()); 
//                    huidigVoertuigNaam = voertuigNaam;
//                    huidigVoertuig.VoegEenToe();//reeds één voertuig
//                }
//                else//hetzelfde type voertuig als in de vorige iteratie
//                {
//                    if (huidigVoertuig != null)
//                        huidigVoertuig.VoegEenToe();//verhoog het aantal voertuigen van dit type met één
//                    else
//                        Console.WriteLine("ERROR: VoertuigReservatieInfo nog niet aangemaakt voor: " + voertuigNaam);
//                }
//            }
//            if (huidigVoertuig != null)//als we een type voertuig hebben, voeg dit (nog) toe aan de returnlijst
//                voertuigen.Add(huidigVoertuig);
//            //TODO: controle of deze allemaal beschikbaar zijn voor deze datum!!!

//            return voertuigen;
//        }

//        /***************************** LOCATIES *********************************************/

//        public List<Locatie> GetVerhuurLocaties()
//        {
//            if (ctx == null)
//                return null;

//            List<Locatie> locaties = new List<Locatie>();
//            List list = ctx.Web.Lists.GetByTitle("Locaties");

//            StringBuilder caml = new StringBuilder();
//            caml.Append("<View><Query>");
//            caml.Append("<Where><And><Eq><FieldRef Name='Actief'/>");
//            caml.Append("<Value Type='Integer'>1</Value></Eq>");
//            caml.Append("<Eq><FieldRef Name='Planningslocatie'/>");
//            caml.Append("<Value Type='Integer'>1</Value></Eq></And></Where>");
//            caml.Append("</Query><RowLimit>100</RowLimit></View>");

//            CamlQuery query = new CamlQuery();
//            query.ViewXml = caml.ToString();

//            ListItemCollection items = list.GetItems(query);
//            ctx.Load(items);
//            ctx.ExecuteQuery();

//            int i = 0;
//            foreach (ListItem item in items)
//            {
//                locaties.Add(new Locatie(item.FieldValues["Title"].ToString(), item.FieldValues["ID"].ToString()));
//                i++;
//            }

//            return locaties;
//        }

//        /******************************* ONDERAANNEMERS *******************************/
//        public List<Onderaannemer> GetOnderaannemers()
//        {
//            if (ctx == null)
//                return null;

//            List<Onderaannemer> aannemers = new List<Onderaannemer>();
//            List myList = ctx.Web.Lists.GetByTitle("Onderaannemer");

//            StringBuilder caml = new StringBuilder();
//            caml.Append("<View><Query>");
//            caml.Append("<OrderBy><FieldRef Name='Title'/>");
//            caml.Append("</OrderBy></Query><RowLimit>100</RowLimit></View>");

//            CamlQuery query = new CamlQuery();
//            query.ViewXml = caml.ToString();

//            ListItemCollection myItems = myList.GetItems(query);
//            ctx.Load(myItems);
//            ctx.ExecuteQuery();

//            foreach (ListItem myItem in myItems)
//            {
//                aannemers.Add(new Onderaannemer((int)myItem.FieldValues["ID"], myItem.FieldValues["Title"].ToString()));
//            }

//            return aannemers;
//        }

//        //Ophalen van ééN onderaannemer via lookupId
//        private Onderaannemer GetOnderaannemerById(ListItem myItem)
//        {
//            if (ctx == null)
//                return null;

//            Onderaannemer aannemer = new Onderaannemer();
//            List list = ctx.Web.Lists.GetByTitle("Onderaannemer");

//            FieldLookupValue fl = (FieldLookupValue)myItem["Geboekt_x0020_via"];
//            if (fl != null)
//            {
//                String onderaannemerId = fl.LookupId.ToString();

//                StringBuilder caml = new StringBuilder();
//                caml.Append("<View><Query>");
//                caml.Append("<Where>");
//                caml.Append("<Eq><FieldRef Name='ID'/>");
//                caml.Append("<Value Type='Integer'>" + onderaannemerId + "</Value></Eq>");
//                caml.Append("</Where>");
//                caml.Append("</Query><RowLimit>1</RowLimit></View>");

//                CamlQuery query = new CamlQuery();
//                query.ViewXml = caml.ToString();

//                ListItemCollection items = list.GetItems(query);
//                ctx.Load(items);
//                ctx.ExecuteQuery();

//                foreach (ListItem item in items)
//                {
//                    aannemer.ID = Convert.ToInt32(item["ID"]);
//                    aannemer.Naam = item["Naam"].ToString();
//                    aannemer.Straat = item["Straat"].ToString();
//                    aannemer.Huisnummer = item["Huisnummer"].ToString();
//                    aannemer.Gemeente = item["Plaats"].ToString();
//                    aannemer.Postcode = item["Postcode"].ToString();
//                    aannemer.Land = item["Land"].ToString();
//                    if (item["BTW_x002d_nummer"] != null)
//                        aannemer.BTWNummer = item["BTW_x002d_nummer"].ToString();
//                    if (item["Telefoon"] != null)
//                        aannemer.Telefoon = item["Telefoon"].ToString();
//                    if (item["Fax"] != null)
//                        aannemer.Fax = item["Fax"].ToString();
//                    if (item["E_x002d_mail"] != null)
//                        aannemer.Email = item["E_x002d_mail"].ToString();

//                }
//                return aannemer;
//            }
//            return null;
//        }

//        /********************************* ANDERE **********************************/
//        //Ophalen van andere info, returns een lijst van andere
//        public List<Andere> GetAndere()
//        {
//            if (ctx == null)
//                return null;

//            List<Andere> andere = new List<Andere>();
//            List myList = ctx.Web.Lists.GetByTitle("Andere");

//            StringBuilder caml = new StringBuilder();
//            caml.Append("<View><Query>");
//            //caml.Append("<Where><Eq><FieldRef Name='PlanningsLocatie'/>");
//            //caml.Append("<Value Type='Integer'>1</Value></Eq></Where>");
//            caml.Append("<OrderBy><FieldRef Name='Title'/>");
//            caml.Append("</OrderBy></Query><RowLimit>100</RowLimit></View>");

//            CamlQuery query = new CamlQuery();
//            query.ViewXml = caml.ToString();

//            ListItemCollection myItems = myList.GetItems(query);
//            ctx.Load(myItems);
//            ctx.ExecuteQuery();

//            foreach (ListItem myItem in myItems)
//            {
//                andere.Add(new Andere(myItem.FieldValues["Title"].ToString(),
//                    myItem.FieldValues["Prijs"].ToString()));

//            }

//            return andere;
//        }

//        /********************************** HORECA ************************************/
//        //Ophalen van de horeca info, returns een lijst van horeca objecten
//        public List<Horeca> GetHorecaInfo()
//        {
//            if (ctx == null)
//                return null;

//            List<Horeca> horeca = new List<Horeca>();
//            List myList = ctx.Web.Lists.GetByTitle("Horeca");

//            StringBuilder caml = new StringBuilder();
//            caml.Append("<View><Query>");
//            caml.Append("OrderBy><FieldRef Name='Type_x0020_horeca'/></OrderBy>");
//            caml.Append("</Query><RowLimit>100</RowLimit></View>");

//            CamlQuery query = new CamlQuery();
//            query.ViewXml = caml.ToString();

//            ListItemCollection myItems = myList.GetItems(query);
//            ctx.Load(myItems);
//            ctx.ExecuteQuery();

//            foreach (ListItem myItem in myItems)
//            {
//                horeca.Add(new Horeca(myItem.FieldValues["Type_x0020_horeca"].ToString(),
//                    myItem.FieldValues["Prijs"].ToString()));
//            }
//            return horeca;
//        }

//        /********************************** GPS ****************************************/
//        //Ophalen van de GPS info, returns een lijst van GPS objecten
//        public List<GPS> GetGPSInfo(String locatie)
//        {
//            if (ctx == null)
//                return null;

//            List<GPS> gpssen = new List<GPS>();
//            List myList = ctx.Web.Lists.GetByTitle("GPS");

//            StringBuilder caml = new StringBuilder();
//            caml.Append("<View><Query>");
//            caml.Append("<OrderBy><FieldRef Name='Type_x0020_GPS'/></OrderBy>");
//            caml.Append("</Query><RowLimit>100</RowLimit></View>");

//            CamlQuery query = new CamlQuery();
//            query.ViewXml = caml.ToString();

//            ListItemCollection myItems = myList.GetItems(query);
//            ctx.Load(myItems);
//            ctx.ExecuteQuery();

//            foreach (ListItem myItem in myItems)
//            {
//                gpssen.Add(new GPS((int)myItem.FieldValues["Title"],
//                    myItem.FieldValues["Type_x0020_GPS"].ToString(),
//                    myItem.FieldValues["Prijs"].ToString(),
//                    myItem.FieldValues["Locatie"].ToString(),
//                    myItem.FieldValues["Opmerking"].ToString(),
//                    myItem.FieldValues["BTW_x002d_percentage"].ToString()));
//            }

//            return gpssen;

//        }

//        /********************************* GIDS ***************************************/
//        //Ophalen van de gidsen info, returns een lijst van Gids objecten
//        public List<Gids> GetGidsInfo(String locatie)
//        {
//            if (ctx == null)
//                return null;

//            List<Gids> gidsen = new List<Gids>();
//            List myList = ctx.Web.Lists.GetByTitle("GPS");

//            StringBuilder caml = new StringBuilder();
//            caml.Append("<View><Query>");
//            caml.Append("<OrderBy><FieldRef Name='Naam'/></OrderBy>");
//            caml.Append("</Query><RowLimit>100</RowLimit></View>");

//            CamlQuery query = new CamlQuery();
//            query.ViewXml = caml.ToString();

//            ListItemCollection myItems = myList.GetItems(query);
//            ctx.Load(myItems);
//            ctx.ExecuteQuery();

//            foreach (ListItem myItem in myItems)
//            {
//                gidsen.Add(new Gids(myItem.FieldValues["Title"].ToString(),
//                    myItem.FieldValues["Type_x0020_gids"].ToString(),
//                    myItem.FieldValues["Prijs"].ToString(),
//                    myItem.FieldValues["Onderaannemer"].ToString(),
//                    myItem.FieldValues["Uitleg"].ToString(),
//                    myItem.FieldValues["Email"].ToString(),
//                    myItem.FieldValues["Locatie"].ToString(),
//                    myItem.FieldValues["Opmerking"].ToString(),
//                    myItem.FieldValues["BTW_x002d_percentage"].ToString()));
//            }
//            return gidsen;
//        }

//        /********************************** KINDERSTOELTJES ****************************/
//        //Ophalen van de informatie over kinderstoeltjes, returns een lijst van Kinderstoeltjes info
//        public List<KinderStoeltje> GetKinderstoeltjesInfo(String locatie)
//        {
//            if (ctx == null)
//                return null;

//            List<KinderStoeltje> kinderStoeltjes = new List<KinderStoeltje>();
//            List myList = ctx.Web.Lists.GetByTitle("GPS");

//            StringBuilder caml = new StringBuilder();
//            caml.Append("<View><Query>");
//            caml.Append("<OrderBy><FieldRef Name='Nummer'/></OrderBy>");
//            caml.Append("</Query><RowLimit>100</RowLimit></View>");

//            CamlQuery query = new CamlQuery();
//            query.ViewXml = caml.ToString();

//            ListItemCollection myItems = myList.GetItems(query);
//            ctx.Load(myItems);
//            ctx.ExecuteQuery();

//            foreach (ListItem myItem in myItems)
//            {
//                kinderStoeltjes.Add(new KinderStoeltje((int)myItem.FieldValues["Title"],
//                    myItem.FieldValues["Locatie"].ToString(),
//                    myItem.FieldValues["Opmerking"].ToString(),
//                    myItem.FieldValues["Prijs"].ToString(),
//                    myItem.FieldValues["BTW_x002d_percentage"].ToString()));
//            }
//            return kinderStoeltjes;
//        }

//        /***************************** VoertuigReservatie ******************************/
//        //Ophalen van de VoertuigReservaties
//        public List<VoertuigReservatie> GetVoertuigReservaties()
//        {
//            if (ctx == null)
//                return null;
//            List<VoertuigReservatie> reservaties = new List<VoertuigReservatie>();
//            List myList = ctx.Web.Lists.GetByTitle("Voertuigreservatie");

//            StringBuilder caml = new StringBuilder();
//            caml.Append("<View><Query>");
//            //caml.Append("<Where>");
//            //caml.Append("<Eq><FieldRef Name='Datum'/>");
//            //caml.Append("<Value Type='String'>" + "datum" + "</Value></Eq>");
//            //caml.Append("</Where>");
//            caml.Append("</Query><RowLimit>100</RowLimit></View>");

//            CamlQuery query = new CamlQuery();
//            query.ViewXml = caml.ToString();

//            ListItemCollection myItems = myList.GetItems(query);
//            ctx.Load(myItems);
//            ctx.ExecuteQuery();

//            foreach (ListItem myItem in myItems)
//            {
//                //ophalen van de Reservatiefiche van elk VoertuigReservatie item
//                ReservatieFiche fiche = new ReservatieFiche();
//                fiche = GetFicheById(myItem);

//                VoertuigReservatie reservatie = new VoertuigReservatie();
//                DateTime theDatum = Convert.ToDateTime(myItem.FieldValues["Datum"].ToString());
//                theDatum = TimeZoneInfo.ConvertTime(theDatum, TimeZoneInfo.Local, TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time"));
//                String myDate = myItem.FieldValues["Datum"].ToString();
//                reservatie.Datum = theDatum.ToShortDateString();
//                reservatie.Fiche = fiche;
//                reservatie.ReservatieType = myItem.FieldValues["Type_x0020_reservatie"].ToString();
//                reservatie.TypeVoertuig = myItem.FieldValues["Type_x0020_voertuig"].ToString();
//                reservatie.Voertuig = myItem.FieldValues["Voertuig"].ToString();
//                reservatie.Aantal = Convert.ToInt32(myItem.FieldValues["Aantal"]);
//                if (myItem.FieldValues["Opmerking"] != null)
//                    reservatie.Opmerking = myItem.FieldValues["Opmerking"].ToString();
//                reservatie.ZelfTanken = (Boolean)myItem.FieldValues["Zelf_x0020_tanken"];
//                reservatie.GeenSchade = Convert.ToInt32(myItem.FieldValues["Geen_x0020_schadeafkoop"]);
//                reservatie.Schade15 = Convert.ToInt32(myItem.FieldValues["_x0031_5_x0025__x0020_schadeafko"]);
//                reservatie.Schade25 = Convert.ToInt32(myItem.FieldValues["_x0032_5_x0025__x0020_schadeafko"]);


//                reservaties.Add(reservatie);
//            }
//            return reservaties;
//        }


//        /********************************** KLANT **************************************/
//        //Opslaan van een klantgegevens
//        public Klant SlaKlantOp(Klant klant)
//        {
//            if (ctx == null)
//                return null;

//            List list = ctx.Web.Lists.GetByTitle("Klanten");
//            ListItemCreationInformation itemCreateInfo = new ListItemCreationInformation();
//            ListItem item = list.AddItem(itemCreateInfo);
//            item["Title"] = klant.Voornaam;
//            item["Achternaam"] = klant.Achternaam;
//            item["Geboortedatum"] = klant.GeboorteDatum;
//            item["GSM_x002f_Tel"] = klant.TelefoonNummer;
//            item["Email"] = klant.Email;
//            item["Correspondentie_x0020_via_x0020_"] = klant.MailCorrespondentie;
//            item["Straat"] = klant.Straat;
//            item["Huisnummer"] = klant.Huisnummer;
//            item["Busnummer"] = klant.Busnummer;
//            item["Postcode"] = klant.Postcode;
//            item["Plaats"] = klant.Gemeente;
//            item["Land"] = klant.Land;
//            item["Type_x0020_klant"] = klant.KlantType;
//            item["Bedrijfsnaam"] = klant.BedrijfsNaam;
//            item["Bedrijfsvorm"] = klant.BedrijfsVorm;
//            item["BTW_x002d_nummer"] = klant.BTWNummer;
//            item["Facturatie_x0020_straat"] = klant.FactuurStraat;
//            item["Facturatie_x0020_huisnummer"] = klant.FactuurHuisNummer;
//            item["Facturatie_x0020_busnummer"] = klant.FactuurBusNummer;
//            item["Facturatie_x0020_postcode"] = klant.FactuurPostcode;
//            item["Facturatie_x0020_stad"] = klant.FactuurGemeente;
//            item["Facturatie_x0020_land"] = klant.FactuurLand;
//            item["Type_x0020_gebruiker"] = "Telefoon";
//            item["Opmerking"] = klant.Opmerking;

//            item.Update();
//            ctx.ExecuteQuery();

//            ctx.Load(item);
//            ctx.ExecuteQuery();
//            klant.ID = item["ID"].ToString();

//            return klant;
//        }

//        //Opvragen van één klant adv ID
//        private Klant GetKlantById(ListItem myItem)
//        {
//            if (ctx == null)
//                return null;
//            Klant klant = new Klant();
//            List list = ctx.Web.Lists.GetByTitle("Klanten");

//            FieldLookupValue fl = (FieldLookupValue)myItem["Klant"];
//            String klantID = fl.LookupId.ToString();

//            StringBuilder caml = new StringBuilder();
//            caml.Append("<View><Query>");
//            caml.Append("<Where>");
//            caml.Append("<Eq><FieldRef Name='ID'/>");
//            caml.Append("<Value Type='Integer'>" + klantID + "</Value></Eq>");
//            caml.Append("</Where>");
//            caml.Append("</Query><RowLimit>1</RowLimit></View>");

//            CamlQuery query = new CamlQuery();
//            query.ViewXml = caml.ToString();

//            ListItemCollection items = list.GetItems(query);
//            ctx.Load(items);
//            ctx.ExecuteQuery();

//            foreach (ListItem item in items)
//            {
//                klant.ID = klantID.ToString();
//                klant.Voornaam = item["Title"].ToString();
//                klant.Achternaam = item["Achternaam"].ToString();
//                klant.TelefoonNummer = item["GSM_x002f_Tel"].ToString();
//                klant.Email = item["Email"].ToString();
//                klant.MailCorrespondentie = Convert.ToBoolean(item["Correspondentie_x0020_via_x0020_"]);
//                if (item["Straat"] != null)
//                    klant.Straat = item["Straat"].ToString();
//                if (item["Huisnummer"] != null)
//                    klant.Huisnummer = item["Huisnummer"].ToString();
//                if (item["Busnummer"] != null)
//                    klant.Busnummer = item["Busnummer"].ToString();
//                if (item["Postcode"] != null)
//                    klant.Postcode = item["Postcode"].ToString();
//                if (item["Plaats"] != null)
//                    klant.Gemeente = item["Plaats"].ToString();
//                if (item["Land"] != null)
//                    klant.Land = item["Land"].ToString();
//                klant.KlantType = item["Type_x0020_klant"].ToString();
//                if (item["Bedrijfsnaam"] != null)
//                    klant.BedrijfsNaam = item["Bedrijfsnaam"].ToString();
//                if (item["Bedrijfsvorm"]  != null)
//                    klant.BedrijfsVorm = item["Bedrijfsvorm"].ToString();
//                if (item["BTW_x002d_nummer"] != null)
//                    klant.BTWNummer = item["BTW_x002d_nummer"].ToString();
//                if (item["Facturatie_x0020_straat"] != null)
//                    klant.FactuurStraat = item["Facturatie_x0020_straat"].ToString();
//                if (item["Facturatie_x0020_huisnummer"] != null)
//                    klant.FactuurHuisNummer = item["Facturatie_x0020_huisnummer"].ToString();
//                if (item["Facturatie_x0020_busnummer"] != null)
//                    klant.FactuurBusNummer = item["Facturatie_x0020_busnummer"].ToString();
//                if (item["Facturatie_x0020_postcode"] != null)
//                    klant.FactuurPostcode = item["Facturatie_x0020_postcode"].ToString();
//                if (item["Facturatie_x0020_stad"] != null)
//                    klant.FactuurGemeente = item["Facturatie_x0020_stad"].ToString();
//                if (item["Facturatie_x0020_land"] != null)
//                    klant.FactuurLand = item["Facturatie_x0020_land"].ToString();
//                if (item["Opmerking"] != null)
//                    klant.Opmerking = item["Opmerking"].ToString();
//                if (item["Type_x0020_gebruiker"] != null)
//                    klant.GebruikerType = item["Type_x0020_gebruiker"].ToString();
//                klant.GeboorteDatum = Convert.ToDateTime(item["Geboortedatum"]);

//            }

//            return klant;
//        }


//        /********************************* Fiche **********************************/
//        //Fiche gegevens opslaan in sharepoint
//        public Boolean SlaFicheOp(ReservatieFiche fiche)
//        {
//            if (ctx == null)
//                return false;

//            try
//            {
//                List list = ctx.Web.Lists.GetByTitle("Fiche");
//                ListItemCreationInformation itemCreateInfo = new ListItemCreationInformation();
//                ListItem item = list.AddItem(itemCreateInfo);
//                item["Klant"] = fiche.Klant.ID;
//                item["Datum"] = Convert.ToDateTime(fiche.Datum);
//                item["Geen_x0020_datum"] = fiche.GeenDatum;
//                item["Aantal_x0020_personen"] = fiche.AantalPersonen;
//                item["Mannen"] = fiche.Mannen;
//                item["Vrouwen"] = fiche.Vrouwen;
//                item["Kinderen"] = fiche.Kinderen;
//                item["Geboekt_x0020_via"] = fiche.GeboektVia;
//                item["Cadeaubon"] = fiche.Cadeaubon;

//                item.Update();
//                ctx.ExecuteQuery();
//            }
//            catch (Exception)
//            {
//                return false;
//            }

//            return true;
//        }

//        //Ophalen van de Fiches 
//        public List<ReservatieFiche> GetFiche()
//        {
//            if (ctx == null)
//                return null;

//            List<ReservatieFiche> Fiches = new List<ReservatieFiche>();
//            List myList = ctx.Web.Lists.GetByTitle("Fiche");

//            StringBuilder caml = new StringBuilder();
//            caml.Append("<View><Query>");
//            caml.Append("<OrderBy><FieldRef Name='Datum'/></OrderBy>");
//            caml.Append("</Query><RowLimit>100</RowLimit></View>");

//            CamlQuery query = new CamlQuery();
//            query.ViewXml = caml.ToString();

//            ListItemCollection myItems = myList.GetItems(query);
//            ctx.Load(myItems);
//            ctx.ExecuteQuery();

//            foreach (ListItem myItem in myItems)
//            {
//                ctx.Load(myItem);
//                ctx.ExecuteQuery();

//                Klant klant = new Klant();
//                klant = GetKlantById(myItem);

//                ReservatieFiche fiche = new ReservatieFiche();
//                fiche.Klant = klant;
//                fiche.Datum = myItem.FieldValues["Datum"].ToString();
//                fiche.GeenDatum = (Boolean)myItem.FieldValues["Geen_x0020_datum"];
//                fiche.AantalPersonen = Convert.ToInt32(myItem.FieldValues["Aantal_x0020_personen"]);
//                fiche.Mannen = Convert.ToInt32(myItem.FieldValues["Mannen"]);
//                fiche.Vrouwen = Convert.ToInt32(myItem.FieldValues["Vrouwen"]);
//                fiche.Kinderen = Convert.ToInt32(myItem.FieldValues["Kinderen"]);
//                //fiche.GeboektVia = myItem.FieldValues["Geboekt_x0020_via"].ToString();
//                fiche.Cadeaubon = (Boolean)myItem.FieldValues["Cadeaubon"];

//                Fiches.Add(fiche);
//            }

//            return Fiches;
//        }

//        //Ophalen van één fiche adv het ID
//        private ReservatieFiche GetFicheById(ListItem myItem)
//        {
//            if (ctx == null)
//                return null;
//            ReservatieFiche myFiche = new ReservatieFiche();
//            List list = ctx.Web.Lists.GetByTitle("Fiche");

//            FieldLookupValue fl = (FieldLookupValue)myItem["Fiche"];
//            String ficheID = fl.LookupId.ToString();

//            StringBuilder caml = new StringBuilder();
//            caml.Append("<View><Query>");
//            caml.Append("<Where>");
//            caml.Append("<Eq><FieldRef Name='ID'/>");
//            caml.Append("<Value Type='Integer'>" + ficheID + "</Value></Eq>");
//            caml.Append("</Where>"); 
//            caml.Append("</Query><RowLimit>1</RowLimit></View>");

//            CamlQuery query = new CamlQuery();
//            query.ViewXml = caml.ToString();

//            ListItemCollection items = list.GetItems(query);
//            ctx.Load(items);
//            ctx.ExecuteQuery();

//            foreach (ListItem item in items)
//            {
//                Klant klant = new Klant();
//                klant = GetKlantById(item);

//                Onderaannemer aannemer = new Onderaannemer();
//                aannemer = GetOnderaannemerById(item);

//                myFiche.ID = Convert.ToInt32(ficheID);
//                myFiche.Klant = klant;
//                myFiche.Datum = item["Datum"].ToString();
//                myFiche.GeenDatum = (Boolean)item["Geen_x0020_datum"];
//                myFiche.AantalPersonen = Convert.ToInt32(item["Aantal_x0020_personen"]);
//                myFiche.Mannen = Convert.ToInt32(item["Mannen"]);
//                myFiche.Vrouwen = Convert.ToInt32(item["Vrouwen"]);
//                myFiche.Kinderen = Convert.ToInt32(item["Kinderen"]);
//                myFiche.GeboektVia = aannemer;
//                myFiche.Cadeaubon = Convert.ToBoolean(item["Cadeaubon"]);
//                var status = item["_ModerationStatus"];
//                //myFiche.Status = item["_ModerationStatus"].ToString();
//            }

//            return myFiche;
//        }




//        /****************************************** PLANNINGITEM ***************************************/
//        //Opslaan van het extra planningitem
//        public Boolean SlaPlanningItemOp(PlanningItem myItem)
//        {
//            if (ctx == null)
//                return false;

//            try
//            {
//                List list = ctx.Web.Lists.GetByTitle("Planningitems");
//                ListItemCreationInformation itemCreateInfo = new ListItemCreationInformation();
//                ListItem item = list.AddItem(itemCreateInfo);
//                item["Title"] = myItem.BetrokkenPersoon;
//                item["Type_x0020_planningitem"] = myItem.PlanningItemType;
//                item["Datum"] = myItem.Datum;
//                item["Opmerking"] = myItem.Opmerking;

//                item.Update();
//                ctx.ExecuteQuery();
//            }
//            catch (Exception)
//            {
//                return false;
//            }
//            return true;
//        }

//        //Ophalen van de planningitems
//        public List<PlanningItem> GetPlanningInfo()
//        {
//            if (ctx == null)
//                return null;

//            List<PlanningItem> planningitems = new List<PlanningItem>();
//            List myList = ctx.Web.Lists.GetByTitle("Planningitems");
//            //NIET NODIG ctx.Load(myList);

//            StringBuilder caml = new StringBuilder();
//            caml.Append("<View><Query>");
//            caml.Append("</Query><RowLimit>100</RowLimit></View>");

//            CamlQuery query = new CamlQuery();
//            query.ViewXml = caml.ToString();

//            ListItemCollection myItems = myList.GetItems(query);
//            ctx.Load(myItems);
//            ctx.ExecuteQuery();

//            foreach (ListItem myItem in myItems)
//            {
//                DateTime datum = (DateTime)myItem.FieldValues["Datum"];
//                datum = DateTime.SpecifyKind(datum, DateTimeKind.Local);
//                datum = TimeZoneInfo.ConvertTime(datum, TimeZoneInfo.Local, TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time"));
//                planningitems.Add(new PlanningItem(myItem.FieldValues["Title"].ToString(),
//                    myItem.FieldValues["Type_x0020_planningitem"].ToString(),
//                    datum,
//                    myItem.FieldValues["Opmerking"].ToString()));
//            }
//            return planningitems;
//        }

//        // ************************** TEST DATUM TIJD ***********************/
//        public TestLijstItem GetTestItem()
//        {
//            if (ctx == null)
//                return null;

//            TestLijstItem TheItem = new TestLijstItem();
//            List myList = ctx.Web.Lists.GetByTitle("Testlijst");
//            //NIET NODIG ctx.Load(myList);

//            StringBuilder caml = new StringBuilder();
//            caml.Append("<View><Query>");
//            caml.Append("</Query><RowLimit>1</RowLimit></View>");

//            CamlQuery query = new CamlQuery();
//            query.ViewXml = caml.ToString();

//            ListItemCollection myItems = myList.GetItems(query);
//            ctx.Load(myItems);
//            ctx.ExecuteQuery();

//            foreach (ListItem myItem in myItems)
//            {
//                TheItem.Titel = myItem.FieldValues["Title"].ToString();
//                TheItem.Nu = Convert.ToDateTime(myItem.FieldValues["Nu"]);
//            }
//            return TheItem;
//        }
//    }
//}
