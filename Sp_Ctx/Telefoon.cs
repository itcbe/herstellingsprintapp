﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sp_Ctx
{
    public class Telefoon
    {
        [DataObjectField(true)]
        public String Nummer { get; set; }
        public String Persoon { get; set; }

        public Telefoon() { }

        public Telefoon(String nummer, String persoon)
        {
            this.Nummer = nummer;
            this.Persoon = persoon;
        }
    }
}
