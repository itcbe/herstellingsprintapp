﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HerstellingTicketLibrary
{
    public class Klant
    {
        public int ID { get; set; }
        public String Bedrijf { get; set; }
        public String Telefoon { get; set; }
        public String Email { get; set; }

        public Klant() { }
    }
}
