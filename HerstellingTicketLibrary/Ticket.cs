﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HerstellingTicketLibrary
{
    public class Ticket
    {
        public int ID { get; set; }
        public int KlantID { get; set; }
        public int ContactID { get; set; }
        public Klant Klant { get; set; }
        public Contact Contact { get; set; }
        public String Omschrijving { get; set; }
        public String Type { get; set; }
        public DateTime Datum { get; set; }
        public String Opmerking { get; set; }

        public Ticket() { }
    }
}
