﻿using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace HerstellingTicketLibrary
{
    public class WordDocument
    {
        public Application app;
        public _Document doc;
        public object optional;
        public String titel;

        public enum Alignment { Left, Right, Center };
        public enum Color { Auto, Black, Blue, Red, Green, Yellow };


        public WordDocument(String titel, String orientatie)
        {
            //this.titel = titel;
            try
            {
                app = new Application();
                app.Visible = true;


                // Set up to create a plain, empty text document.
                // Setting these variables to Missing.Value is comparable.
                // to not providing a value for an optional parameter in VB.
                // This gets the default behavior.
                object template = Missing.Value; //No template.
                object newTemplate = Missing.Value; //Not creating a template.
                object documentType = Missing.Value; //Plain old text document.
                object visible = true;  //Show the doc while we work.
                doc = app.Documents.Add(ref template,
                         ref newTemplate,
                         ref documentType,
                         ref visible);
                if (orientatie == "Staand")
                    doc.PageSetup.Orientation = WdOrientation.wdOrientPortrait;
                else
                    doc.PageSetup.Orientation = WdOrientation.wdOrientLandscape;

                

                doc.Words.First.Text = titel + "\n";
            }
            catch (Exception ex)
            {
                ;//TODO toon exception
            }
        }

        public WordDocument(Object template, String orientatie)
        {
            try
            {
                app = new Application();
                app.Visible = true;

                // Set up to create a plain, empty text document.
                // Setting these variables to Missing.Value is comparable.
                // to not providing a value for an optional parameter in VB.
                // This gets the default behavior.
                // object template = Missing.Value; //No template.
                object newTemplate = Missing.Value; //Not creating a template.
                object documentType = Missing.Value; //Plain old text document.
                object visible = true;  //Show the doc while we work.
                doc = app.Documents.Add(ref template,
                         ref newTemplate,
                         ref documentType,
                         ref visible);
                if (orientatie == "Staand")
                    doc.PageSetup.Orientation = WdOrientation.wdOrientPortrait;
                else
                    doc.PageSetup.Orientation = WdOrientation.wdOrientLandscape;

                //doc.Words.First.Text = titel + "\n";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Print()
        {
            foreach (Document document in app.Documents)
            {
                document.PrintOut(false);
            }
            //app.Documents[0].PrintOut(false);
        }

        public void ChangeTicketFields(List<string> fields)
        {
            int index = 1;
            foreach (String field in fields)
            {
                switch (index)
                {
                    case 1:
                        FindAndReplace("#type#", field);
                        break;
                    case 2:
                        FindAndReplace("#ticket#", field);
                        break;
                    case 3:
                        FindAndReplace("#datum#", field);
                        break;
                    case 4:
                        FindAndReplace("#klant#", field);
                        break;
                    case 5:
                        FindAndReplace("#contact#", field);
                        break;
                    case 6:
                        FindAndReplace("#telefoon#", field);
                        break;
                    case 7:
                        FindAndReplace("#gsm#", field);
                        break;
                    case 8:
                        FindAndReplace("#email#", field);
                        break;
                    case 9:
                        FindAndReplace("#foutmelding#", field);
                        break;
                    case 10:
                        FindAndReplace("#opmerking#", field);
                        break;
                }
                index++;
            }
        }

        public void FindAndReplace(object findtext, object replacetext)
        {
            try
            {
                object matchCase = true;
                object matchWholeWord = true;
                object matchWildCards = false;
                object matchSoundLike = false;
                object nmatchAllForms = false;
                object forward = true;
                object format = false;
                object matchKashida = false;
                object matchDiactitics = false;
                object matchAlefHamza = false;
                object matchControl = false;
                object read_only = false;
                object visible = true;
                object replace = 2;
                object wrap = 1;

                this.app.Selection.Find.Execute(ref findtext,
                    ref matchCase,
                    ref matchWholeWord,
                    ref matchWildCards,
                    ref matchSoundLike,
                    ref nmatchAllForms,
                    ref forward,
                    ref wrap,
                    ref format,
                    ref replacetext,
                    ref replace,
                    ref matchKashida,
                    ref matchDiactitics,
                    ref matchAlefHamza,
                    ref matchControl);

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public Boolean addLine(String line)
        {
            try
            {
                doc.Words.Last.Text = line + "\n";
                return true;
            }
            catch (Exception)//TODO toon exception
            {
                return false;
            }
        }

        public Boolean addInlinePicture(String pad)
        {
            try
            {
                Object range = doc.Words.Last; //the position you want to insert
                Object oLinkToFile = false;  //default
                Object oSaveWithDocument = true;//default
                doc.InlineShapes.AddPicture(pad, ref oLinkToFile, ref oSaveWithDocument, ref range);
                return true;
            }
            catch (Exception)//TODO toon exception
            {
                return false;
            }
        }

        public Boolean addPictureRight(String pad, int width, int height)
        {
            try
            {
                doc.Words.Last.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphRight;
                Object range = doc.Words.Last; //the position you want to insert
                Object oLinkToFile = false;  //default
                Object oSaveWithDocument = true;//default
                Microsoft.Office.Interop.Word.InlineShape imageInserted = doc.InlineShapes.AddPicture(pad, ref oLinkToFile, ref oSaveWithDocument, ref range);
                Microsoft.Office.Interop.Word.Shape shape = imageInserted.ConvertToShape();
                shape.Width = width;
                shape.Height = height;
                shape.WrapFormat.Type = Microsoft.Office.Interop.Word.WdWrapType.wdWrapSquare;
                shape.Left = (float)Microsoft.Office.Interop.Word.WdShapePosition.wdShapeRight;
                shape.Top = (float)Microsoft.Office.Interop.Word.WdShapePosition.wdShapeTop;
                return true;
            }
            catch (Exception)//TODO toon exception
            {
                return false;
            }
        }

        public Boolean addHorizontalLine()
        {
            try
            {
                Object range = doc.Words.Last;
                doc.InlineShapes.AddHorizontalLineStandard(range);
                return true;
            }
            catch (Exception) //TODO toon exception
            {
                return false;
            }
        }

        //BulletList
        public Boolean addBulletList(String[,] bulletItems)
        {

            try
            {
                //this.toggleUnderline();
                Paragraph assets = doc.Content.Paragraphs.Add();
                assets.Range.ListFormat.ApplyBulletDefault();

                for (int i = 0; i < bulletItems.Length; i++)
                {
                    if (i == 0)
                        assets.TabIndent(-1);
                    if (bulletItems[i, 0] != null)
                    {
                        string bulletItem = bulletItems[i, 0];
                        if (i < bulletItems.Length - 1)
                            bulletItem = bulletItem + "\n";
                        assets.Range.InsertBefore(bulletItem);
                        assets.TabIndent(1);
                        //this.toggleUnderline();
                        for (int j = 1; j < 4; j++)
                        {
                            if (!String.IsNullOrEmpty(bulletItems[i, j]))
                            {
                                string bulletItem2 = bulletItems[i, j] + "\n";
                                assets.Range.InsertBefore(bulletItem2);

                            }
                        }
                        assets.TabIndent(-1);
                    }
                }
                //this.toggleUnderline();
                return true;
            }
            catch (Exception)
            {
                //this.toggleUnderline();
                return false;
            }
        }

        //Table
        public Boolean addTable(String[][] tableContent, Boolean titleRow = false, int[] colWidth = null, int colheight = 0, int sprekers = 0)
        {
            try
            {
                Table table;
                System.Drawing.Color c = System.Drawing.Color.FromArgb(184, 204, 228);
                WdColor lichtBlue = (WdColor)(c.R + 0x100 * c.G + 0x10000 * +c.B);
                //tableContent

                object oVisible = true;

                if (tableContent == null || tableContent.Length == 0 || tableContent[0].Length == 0)
                    return false;

                table = doc.Tables.Add(doc.Words.Last, tableContent.Length, tableContent[0].Length - 1);

                for (int i = 0; i < tableContent.Length; i++)
                {
                    if (tableContent[i][4] == "0")
                    {
                        table.Rows[i + 1].Cells.Shading.BackgroundPatternColorIndex = WdColorIndex.wdWhite;
                    }
                    else
                        table.Rows[i + 1].Cells.Shading.BackgroundPatternColor = lichtBlue;

                    //table.Rows[i].Range.Font.Size = 11;
                    for (int j = 0; j < tableContent[0].Length - 1; j++)
                    {
                        var tekst = tableContent[i][j];
                        if (tekst == "z")
                            tekst = "";
                        table.Cell(i + 1, j + 1).Range.InsertAfter(tekst);
                    }
                }

                if (colWidth != null)
                {
                    for (int i = 0; i < colWidth.Length; i++)
                    {
                        table.Columns[i + 1].SetWidth(colWidth[i], WdRulerStyle.wdAdjustNone);
                    }
                }

                table.Rows[1].HeadingFormat = -1;

                if (titleRow)
                {
                    table.Rows[1].Cells.Shading.BackgroundPatternColorIndex = WdColorIndex.wdWhite;
                    table.Rows[1].Range.Bold = 1;
                    table.Rows[1].Range.Paragraphs.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                }

                table.Borders.InsideLineStyle = WdLineStyle.wdLineStyleSingle;
                table.Borders.OutsideLineStyle = WdLineStyle.wdLineStyleSingle;
                table.Rows.SetHeight(colheight, WdRowHeightRule.wdRowHeightExactly);


                doc.Words.Last.Text = "\n";
                return true;
            }
            catch (Exception)//TODO toon exception
            {
                return false;
            }
        }

        public Boolean addAttestTable(string[][] tableContent, bool titleRow, int[] colWidth, int colheight, int sprekers, string attesttype)
        {
            try
            {
                Table table;
                System.Drawing.Color c = System.Drawing.Color.FromArgb(184, 204, 228);
                WdColor lichtBlue = (WdColor)(c.R + 0x100 * c.G + 0x10000 * +c.B);

                object oVisible = true;

                if (tableContent == null || tableContent.Length == 0 || tableContent[0].Length == 0)
                    return false;

                table = doc.Tables.Add(doc.Words.Last, tableContent.Length + 1, tableContent[0].Length - 1);
                if (colWidth != null)
                {
                    for (int i = 0; i < colWidth.Length; i++)
                    {
                        table.Columns[i + 1].SetWidth(colWidth[i], WdRulerStyle.wdAdjustNone);
                    }
                }
                table.Rows[1].Cells[5].Merge(table.Rows[1].Cells[6]);
                table.Cell(1, 5).Range.InsertAfter(attesttype);
                table.Rows[1].Range.Bold = 1;
                table.Rows[1].Cells[5].Borders.OutsideLineStyle = WdLineStyle.wdLineStyleSingle;
                table.Rows[1].Cells[5].Borders.OutsideLineWidth = WdLineWidth.wdLineWidth050pt;
                for (int i = 0; i < tableContent.Length; i++)
                {
                    if (tableContent[i][6] == "0")
                    {
                        table.Rows[i + 2].Cells.Shading.BackgroundPatternColorIndex = WdColorIndex.wdWhite;
                    }
                    else
                        table.Rows[i + 2].Cells.Shading.BackgroundPatternColor = lichtBlue;

                    //table.Rows[i].Range.Font.Size = 11;
                    for (int j = 0; j < tableContent[0].Length - 1; j++)
                    {
                        var tekst = tableContent[i][j];
                        if (tekst == "z")
                            tekst = "";
                        table.Cell(i + 2, j + 1).Range.InsertAfter(tekst);
                    }
                    table.Rows[i + 2].Borders.InsideLineStyle = WdLineStyle.wdLineStyleSingle;
                    table.Rows[i + 2].Borders.OutsideLineStyle = WdLineStyle.wdLineStyleSingle;

                }

                table.Rows[2].HeadingFormat = -1;


                if (titleRow)
                {
                    table.Rows[2].Cells.Shading.BackgroundPatternColorIndex = WdColorIndex.wdWhite;
                    table.Rows[2].Range.Bold = 1;
                    table.Rows[2].Range.Paragraphs.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                    table.Rows[2].Borders.OutsideLineStyle = WdLineStyle.wdLineStyleSingle;
                    table.Rows[2].Borders.InsideLineStyle = WdLineStyle.wdLineStyleSingle;
                }

                table.Rows.SetHeight(colheight, WdRowHeightRule.wdRowHeightExactly);


                doc.Words.Last.Text = "\n";
                return true;
            }
            catch (Exception ex)//TODO toon exception
            {
                return false;
            }

        }


        //layout/format
        public Boolean setAlignment(Alignment alignment)
        {
            try
            {
                switch (alignment)
                {
                    case Alignment.Center:
                        doc.Words.Last.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                        break;
                    case Alignment.Right:
                        doc.Words.Last.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphRight;
                        break;
                    case Alignment.Left:
                    default:
                        doc.Words.Last.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphLeft;
                        break;

                }

                return true;
            }
            catch (Exception)
            {
                return false;//TODO toon exception
            }
        }

        public Boolean toggleUnderline()
        {
            try
            {
                if (doc.Words.Last.Underline == 0)
                    doc.Words.Last.Underline = WdUnderline.wdUnderlineSingle;
                else
                    doc.Words.Last.Underline = WdUnderline.wdUnderlineNone;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Boolean setFontsize(int size)
        {
            try
            {
                doc.Words.Last.Font.Size = size;
                return true;
            }
            catch (Exception) // TODO toon exception
            {
                return false;
            }
        }

        public Boolean setTextColor(Color color)
        {
            try
            {
                switch (color)
                {
                    case Color.Black:
                        doc.Words.Last.Font.ColorIndex = WdColorIndex.wdBlack;
                        break;
                    case Color.Blue:
                        doc.Words.Last.Font.ColorIndex = WdColorIndex.wdBlue;
                        break;
                    case Color.Red:
                        doc.Words.Last.Font.ColorIndex = WdColorIndex.wdRed;
                        break;
                    case Color.Yellow:
                        doc.Words.Last.Font.ColorIndex = WdColorIndex.wdYellow;
                        break;
                    case Color.Green:
                        doc.Words.Last.Font.ColorIndex = WdColorIndex.wdGreen;
                        break;
                    case Color.Auto:
                    default:
                        doc.Words.Last.Font.ColorIndex = WdColorIndex.wdAuto;
                        break;
                }
                return true;
            }
            catch (Exception) //TODO toon exception
            {
                return false;
            }
        }

        public Boolean toggleBold()
        {
            try
            {
                if (doc.Words.Last.Font.Bold == 0)
                    doc.Words.Last.Font.Bold = 1;
                else
                    doc.Words.Last.Font.Bold = 0;
                return true;
            }
            catch (Exception) //TODO toon exception
            {
                return false;
            }
        }

        //actions
        public Boolean save(String path)
        {
            //TODO folder via instellingen
            //Save the file, use default values except for filename.
            object fileName = path;
            optional = Missing.Value;  //Take default values.

            try
            {
#if OFFICEXP
                              doc.SaveAs2000( ref fileName,
#else
                doc.SaveAs(ref fileName,
#endif
                 ref optional, ref optional, ref optional, ref optional, ref optional, ref optional, ref optional, ref optional, ref optional, ref optional);
                return true;
            }
            catch (Exception)//TODO toon exception
            {
                return false;
            }
        }

        public void Activate()
        {
            doc.Activate();
        }

        public Boolean quit()
        {
            try
            {
                // Now use the Quit method to cleanup.
                object saveChanges = false;
                app.Quit(ref saveChanges, ref optional, ref optional);
                return true;
            }
            catch (Exception)//TODO toon exception
            {
                return false;
            }
        }

        public bool SaveAsPDF(string path)
        {
            //TODO folder via instellingen
            //Save the file, use default values except for filename.
            object fileName = path;
            optional = Missing.Value;  //Take default values.
            object fileFormat = WdSaveFormat.wdFormatPDF;
            try
            {
                doc.ExportAsFixedFormat(path, WdExportFormat.wdExportFormatPDF, false, WdExportOptimizeFor.wdExportOptimizeForOnScreen, WdExportRange.wdExportAllDocument, 1, 1, WdExportItem.wdExportDocumentContent, true, false, WdExportCreateBookmarks.wdExportCreateHeadingBookmarks, true, true, false, ref optional);
                return true;
            }
            catch (Exception ex)//TODO toon exception
            {
                return false;
            }

        }

        public bool CopyTable(int aantal)
        {
            var mydoc = app.Documents[1];
            Range range = mydoc.Tables[2].Range;
            range.Copy();
            for (int i = 1; i < aantal; i++)
            {
                range.Paste();
            }
            return true;
        }
    }
}
