﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HerstellingTicketLibrary
{
    public class PrintInfo
    {
        public DateTime Datum { get; set; }
        public String Ticketnummer { get; set; }
        public String Klant { get; set; }
        public String KlantPlaats { get; set; }
        public String KlantService { get; set; }
        public String Contact { get; set; }
        public String ContactTelefoon { get; set; }
        public String ContactGSM { get; set; }
        public String ContactEmail { get; set; }
        public String Foutmelding { get; set; }
        public String Type { get; set; }
        public String Opmerking { get; set; }

        public PrintInfo() { }
    }
}
