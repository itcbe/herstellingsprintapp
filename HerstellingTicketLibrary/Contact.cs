﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HerstellingTicketLibrary
{
    public class Contact
    {
        public int ID { get; set; }
        public String Naam { get; set; }
        public String GSM { get; set; }
        public String Telefoon { get; set; }
        public String Email { get; set; }

        public Contact() { }
    }
}
